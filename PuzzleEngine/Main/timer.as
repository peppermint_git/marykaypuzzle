﻿package main {
	
	import flash.display.*;
	import flash.events.*;
	import flash.utils.*;	
	
	public class timer extends MovieClip {
		public var secsT		:Number = 0;
		var secT				:Number;
		var minT				:Number;
		public var myTimer		:Timer = new Timer(1000, 0);
		public var penaltySig	:Boolean = false;
		
		public function timer() {
			// constructor code
		}
		
		public function fnStart(sec:Number = 15){
			secsT=sec;			
			if(!this.myTimer.hasEventListener(TimerEvent.TIMER)){
				myTimer.addEventListener(TimerEvent.TIMER, stTime);
			}
            myTimer.start();			
		}
		
		public function fnStop(){
			myTimer.stop();
			root.gameOver = true;
		}
		
		function stTime(e:Event){
			secsT--;
			if(secsT<=0){
				secsT=0;
				myTimer.stop();
				root.score = -1;
				root.gameOver = true;
			}
		}
		
		public function penalty(sec:Number=5){
			secsT=secsT-sec;
		}
	}
	
}
