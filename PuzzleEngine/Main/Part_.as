﻿package Main {
	
	import flash.display.*;
	import flash.events.*;
	import flash.geom.Rectangle;
	import flash.utils.*;
	import caurina.transitions.*;
	
	public class Part extends MovieClip {
		//public var ID				:Number;
		
		public var wPos				:Number;
		public var hPos				:Number;
		
		public var fieldW			:Number;
		public var fieldH			:Number;
		
		public var attachedParts	:Array = new Array();
		
		private var drag			:Boolean = false;
		
		private var preX			:Number;
		private var preY			:Number;
		private var isAnimated		:Boolean = false;
		
		private var image			:Bitmap;
		
		public function mcPart(){
			
		}
		
		public function Init(wp:Number,hp:Number){
			trace("Init w: "+wp+" h: "+hp);
			this.wPos = wp;
			this.hPos = hp;
			
			this.attachedParts.push(this)
			
			this.addEventListener(MouseEvent.MOUSE_DOWN, fnStartDrag);
			this.addEventListener(MouseEvent.MOUSE_UP, fnStopDrag);
			
			var mainBMD:BitmapData = root.image.bitmapData; 
			var clonedBMD:BitmapData = mainBMD.clone(); 
			 
			this.image = new Bitmap(clonedBMD,"auto",true);
			
			var puzzlePixW:Number = 125*fieldW;
			var puzzlePixH:Number = 125*fieldH;
			
			this.mcPic.mcPicCont.addChild(this.image);
			
			if(this.image.width/this.image.height>puzzlePixW/puzzlePixH){
				this.mcPic.mcPicCont.height = puzzlePixH;
				this.mcPic.mcPicCont.scaleX = this.mcPic.mcPicCont.scaleY;
			}else{
				this.mcPic.mcPicCont.width = puzzlePixW;
				this.mcPic.mcPicCont.scaleY = this.mcPic.mcPicCont.scaleX;
			}
			
			this.mcPic.mcPicCont.x = -62.5;
			this.mcPic.mcPicCont.y = -62.5;

			/*this.mgT.Init();
			this.mgB.Init();
			this.mgL.Init();
			this.mgR.Init();*/
		}
		
		function fnStartDrag(e:Event){
			this.preX = this.x;
			this.preY = this.y;
			
			
			var ic:Number;
			var ic2:Number = this.parent.getChildIndex(this);
			
			for(ic=0;ic<this.parent.numChildren;ic++){
				if(this.parent.getChildIndex(this.parent.getChildAt(ic))>ic2){
					this.parent.swapChildren(this.parent.getChildAt(ic),this);
				}
			}
			
			this.drag=true;
			
			this.addEventListener(Event.ENTER_FRAME, fnDragCheckTouch);
			this.parent.addEventListener(MouseEvent.MOUSE_UP, fnStopDrag);
		}
		
		private function AnimEnd(){
			this.isAnimated = false;
		}
		
		public function fnStopDrag(e:Event=null){
			if(this.preX == this.x&&this.preY == this.y){
				if(this.attachedParts.length<2&&!this.isAnimated){
					var targrot:Number = this.rotation + 90;
					this.isAnimated = true;
					Tweener.addTween(this,{rotation:targrot, time:0.3,  transition:"easeOutQuad",onComplete:AnimEnd});
					this.parent.removeEventListener(MouseEvent.MOUSE_UP, fnStopDrag);
				}
			}
			
			var ic:Number;
			var ic2:Number;
			var ic3:Number;
			var tempArr:Array = new Array();
			
			var dx:Number;
			var dy:Number;
			var ddx:Number;
			var ddy:Number;
			
			var dMoved:Boolean = false;
			
			trace("wPos: "+this.wPos+" hPos: "+this.hPos);
			
			this.drag=false;
			if(this.hasEventListener(Event.ENTER_FRAME)){
				this.removeEventListener(Event.ENTER_FRAME, fnDragCheckTouch)
			}
			if(this.rotation==0){
				for(ic=0;ic<this.attachedParts.length;ic++){
					for(ic2=0;ic2<this.parent.numChildren;ic2++){
						var isFind = false;
						
						var mcAtt:MovieClip = this.attachedParts[ic];
						var mcOth:MovieClip = this.parent.getChildAt(ic);
						
						var atW:Number = mcAtt.wPos;
						var atH:Number = mcAtt.hPos;
						
						var othW:Number = mcOth.wPos;
						var othH:Number = mcOth.hPos;
						
						if(mcAtt.mgT.Active && atW == othW && atH == (othH+1) && mcAtt.mgT.free){
							if(mcOth.mgB.hitTestObject(mcAtt.mgT)){
								trace("find from T to B connection");
								
								mcAtt.mgT.free = false;
								mcOth.mgB.free = false;
								
								ddx = 0;
								ddy = 125;
								isFind = true;
							}
						}
						if(mcAtt.mgB.Active && atW == othW && atH == (othH-1) && mcAtt.mgB.free){
							if(mcOth.mgT.hitTestObject(mcAtt.mgB)){
								trace("find from B to T connection");
								
								mcAtt.mgB.free = false;
								mcOth.mgT.free = false;
								
								ddx = 0;
								ddy = -125;
								isFind = true;
							}
						}
						if(mcAtt.mgL.Active && atW == (othW+1) && atH == othH && mcAtt.mgL.free){
							if(mcOth.mgR.hitTestObject(mcAtt.mgL)){
								trace("find from L to R connection");
								
								mcAtt.mgL.free = false;
								mcOth.mgR.free = false;
								
								ddx = 125;
								ddy = 0;
								isFind = true;
							}
						}
						if(mcAtt.mgR.Active && atW == (othW-1) && atH == othH && mcAtt.mgR.free){
							if(mcOth.mgL.hitTestObject(mcAtt.mgR)){
								trace("find from R to L connection");
								
								mcAtt.mgR.free = false;
								mcOth.mgL.free = false;
								
								ddx = -125;
								ddy = 0;
								isFind = true;
							}
						}
						
						if(isFind){
							//-------------------------------coord for all connected elems-------
							if(!dMoved){
								dx=mcAtt.x-(mcOth.x+ddx);
								dy=mcAtt.y-(mcOth.y+ddy);
								
								trace("All mooved dx: "+dx+" dy: "+dy);
								
								for(ic3=0;ic3<mcAtt.attachedParts.length;ic3++){
									mcAtt.attachedParts[ic3].x-=dx;
									mcAtt.attachedParts[ic3].y-=dy;
								}
								dMoved=true;
								tempArr = mcAtt.attachedParts.concat(mcOth.attachedParts);
								for(ic3=0;ic3<mcAtt.attachedParts.length;ic3++){
									mcAtt.attachedParts[ic3].attachedParts=tempArr
								}
								for(ic3=0;ic3<mcOth.attachedParts.length;ic3++){
									mcOth.attachedParts[ic3].attachedParts=tempArr
								}
							}
							//--------------------------------------------------------------------
						}
					}
				}
			}
		}

		function fnDragCheckTouch(e:Event){
			if(this.drag){
				for(ic=0;ic<this.attachedParts.length;ic++){
					this.attachedParts[ic].x = this.parent.mouseX - dragPointX[ic];
					this.attachedParts[ic].y = this.parent.mouseY - dragPointY[ic];
				}
			}
		}
		
		public function killListeners(){
			this.removeEventListener(MouseEvent.MOUSE_DOWN, fnStartDrag);
			this.removeEventListener(MouseEvent.MOUSE_UP, fnStopDrag);
		}
	}
}
