﻿package Main {
	
	import flash.display.MovieClip;
	import flash.events.*;
	import caurina.transitions.*;

	public class PlayField extends MovieClip {
		//public var arParts			:Array = new Array();
		public var gameOver			:Boolean = false;
		public var score			:Number = 0;
		public var sideSize			:Number  = 125;
		
		public function mcPlayField() {
			//this.addEventListener(Event.ADDED_TO_STAGE,fAdded);
		}
		
		//----------------------rotate Help--------------------------------
		public function HelpRotate(){
			var ic:Number;
			
			for(ic=0;ic<this.numChildren;ic++){
				if(this.getChildAt(ic).rotation!=0){
					Tweener.addTween(this.getChildAt(ic),{rotation:0, time:0.3,  transition:"easeOutQuad"});
				}
			}
		}
		
		//--------------------generation-------------------------------
		
		public function Init(wdth:Number=4,hght:Number=4){
			var ic:Number;
			var ic2:Number;
			var ic3:Number=0;
			var numToGen:Number = wdth*hght;
			var lineElemType:Number = 0;
			var lineHElemType:Number = 0;
			
			//this.arParts = new Array();
			
			for(ic=0;ic<hght;ic++){
				for(ic2=0;ic2<wdth;ic2++){
					var pobj:Part = new Part();
					var obj:Group = new Group();
					//this.arParts.push(obj);
					this.addChild(obj);
					obj.addChild(pobj);
					
					pobj.fieldW = wdth;
					pobj.fieldH = hght;
					
					var xMod:Number = (stage.stageWidth-this.sideSize);
					var yMod:Number = (stage.stageHeight-this.sideSize);
					
					obj.x = this.sideSize/2+Math.random()*xMod;
					obj.y = this.sideSize/2+Math.random()*yMod;
					var rndObj:Array = new Array(0,90,180,270);
					obj.rotation = rndObj[Math.floor(Math.random()*4)];
					
					//----------------firstLine---------------------
					if(ic==0){
						if(ic2==0){
							pobj.gotoAndStop(1);
						}else{
							if(ic2==wdth-1){
								pobj.gotoAndStop(2);
							}else{
								if(lineElemType==0){
									pobj.gotoAndStop(7);
									lineElemType=1;
								}else{
									pobj.gotoAndStop(8);
									lineElemType=0;
								}
							}
						}
					}else{
						
					//----------------last line------------------
						if(ic==hght-1){
							if(ic2==0){
								pobj.gotoAndStop(4);
							}else{
								if(ic2==wdth-1){
									pobj.gotoAndStop(3);
								}else{
									if(lineElemType==0){
										pobj.gotoAndStop(13);
										lineElemType=1;
									}else{
										pobj.gotoAndStop(14);
										lineElemType=0;
									}
								}
							}
						}else{
					//-------------other Line------------------
							if(ic2==0){
								if(lineHElemType==0){
									pobj.gotoAndStop(9);
								}else{
									pobj.gotoAndStop(10);
								}
							}else{
								if(ic2==wdth-1){
									if(lineHElemType==0){
										pobj.gotoAndStop(12);
									}else{
										pobj.gotoAndStop(11);
									}
								}else{
									if(lineHElemType==0){
										if(lineElemType==0){
											pobj.gotoAndStop(6);
											lineElemType=1;
										}else{
											pobj.gotoAndStop(5);
											lineElemType=0;
										}
									}else{
										if(lineElemType==0){
											pobj.gotoAndStop(5);
											lineElemType=1;
										}else{
											pobj.gotoAndStop(6);
											lineElemType=0;
										}
									}
								}
							}
						}
					}
					pobj.Init(ic2,ic);
					obj.Init();
					pobj.mcPic.x -= ic2*this.sideSize;
					pobj.mcPic.y -= ic*this.sideSize;
				}
				if(ic!=0&&ic!=hght-1){
					if(lineHElemType==0){
						lineHElemType=1;
					}else{
						lineHElemType=0;
					}
				}
			}
		}
//---------------------------------------------------------

	}
}
