﻿package Main {
	
	import flash.display.*;
	import flash.events.*;
	import flash.geom.Rectangle;
	import flash.utils.*;
	import caurina.transitions.*;
	
	public class Part extends MovieClip {
		public var sideSize			:Number  = 125;
		
		public var wPos				:Number;
		public var hPos				:Number;
		
		public var fieldW			:Number;
		public var fieldH			:Number;
		
		private var image			:Bitmap;
		
		
		public function mcPart(){
			
		}
		
		public function Init(wp:Number,hp:Number){
			//trace("Init w: "+wp+" h: "+hp);
			this.wPos = wp;
			this.hPos = hp;
			
			this.setImage();
			
			/*this.mg0.Init();
			this.mg1.Init();
			this.mg2.Init();
			this.mg3.Init();*/
		}
		
		private function setImage(){
			var mainBMD:BitmapData = root.image.bitmapData; 
			var clonedBMD:BitmapData = mainBMD.clone(); 
			 
			this.image = new Bitmap(clonedBMD,"auto",true);
			
			var puzzlePixW:Number = this.sideSize*fieldW;
			var puzzlePixH:Number = this.sideSize*fieldH;
			
			this.mcPic.mcPicCont.addChild(this.image);
			
			if(this.image.width/this.image.height>puzzlePixW/puzzlePixH){
				this.mcPic.mcPicCont.height = puzzlePixH;
				this.mcPic.mcPicCont.scaleX = this.mcPic.mcPicCont.scaleY;
			}else{
				this.mcPic.mcPicCont.width = puzzlePixW;
				this.mcPic.mcPicCont.scaleY = this.mcPic.mcPicCont.scaleX;
			}
			
			this.mcPic.mcPicCont.x = -this.sideSize/2;
			this.mcPic.mcPicCont.y = -this.sideSize/2;
		}
	}
}
