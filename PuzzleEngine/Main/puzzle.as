﻿package Main {
	
	import flash.display.*;
	import flash.utils.*;
	import flash.events.*;
	import flash.net.*;
	import flash.system.*;
	
	public class puzzle extends MovieClip {
		private var xmlPath				: String = "xml/level.xml";
		private var xmlSettings			: XML;
		private var xmlLoader			: URLLoader = new URLLoader();

		private var picpath				: String = "";
		private var puzzleW				: Number = 4;
		private var puzzleH				: Number = 4;
		
		public var image				: Bitmap;
		
		public function puzzle() {
			this.addEventListener(Event.ADDED_TO_STAGE,fAdded);
		}
		
		private function fAdded(e:Event){
			this.xmlLoader.addEventListener(Event.COMPLETE, fXmlLoaded);
			var urlReq:URLRequest = new URLRequest(this.xmlPath);
			this.xmlLoader.load(urlReq);
		}
		
		private function fXmlLoaded(e:Event){
			trace("XML Loaded");
			this.xmlLoader.removeEventListener(Event.COMPLETE, fXmlLoaded);
			this.xmlSettings = new XML(e.target.data);
			
			this.picpath = this.xmlSettings.PICPATH.toString();
			this.puzzleW = parseInt(this.xmlSettings.SIZEW.toString());
			this.puzzleH = parseInt(this.xmlSettings.SIZEH.toString());
			
			var myLoader:Loader = new Loader();
			myLoader.contentLoaderInfo.addEventListener(Event.COMPLETE,fLoadedPic );
			var fileRequest:URLRequest = new URLRequest(this.picpath);
			var lc:LoaderContext = new LoaderContext();
			lc.checkPolicyFile = true;
			myLoader.load(fileRequest, lc);
		}		
		
		private function fLoadedPic(e:Event){
			this.image = (e.target.content as Bitmap);
			this.image.smoothing = true;

			this.Init();
		}
		
		private function onMyPictureLoadError(e:Event){
			trace("ID: "+this.ID+"  error: "+e)
		}


		
		private function Init(){
			this.mcPlayField.Init(this.puzzleW,this.puzzleH);
			this.btnHelpRotate.btn.addEventListener(MouseEvent.MOUSE_DOWN,RotateAll);
		}
		
		private function RotateAll(e:Event){
			this.mcPlayField.HelpRotate();
		}
		
	}
}
