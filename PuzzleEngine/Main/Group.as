﻿package Main {
	
	import flash.display.*;
	import flash.events.*;
	import flash.geom.Rectangle;
	import flash.utils.*;
	import caurina.transitions.*;
	import flash.geom.Point;
	
	
	public class Group extends MovieClip {
		public var fieldW			:Number;
		public var fieldH			:Number;
		
		public var sideSize			:Number  = 125;
		//public var attachedParts	:Array = new Array();
		
		private var drag			:Boolean = false;
		
		private var preX			:Number;
		private var preY			:Number;
		
		private var isAnimated		:Boolean = false;
		
		private var dragPointX		:Number;
		private var dragPointY		:Number;
		
		public function Group() {
			
		}
		
		public function Init(){
			this.addEventListener(MouseEvent.MOUSE_DOWN, fnStartDrag);
			this.addEventListener(MouseEvent.MOUSE_UP, fnStopDrag);
		}
		
		function fnStartDrag(e:Event){
			this.preX = this.x;
			this.preY = this.y;
			
			this.dragPointX = this.parent.mouseX-this.x;
			this.dragPointY = this.parent.mouseY-this.y;
			
			var ic:Number;
			var ic2:Number = this.parent.getChildIndex(this);
			
			for(ic=0;ic<this.parent.numChildren;ic++){
				if(this.parent.getChildIndex(this.parent.getChildAt(ic))>ic2){
					this.parent.swapChildren(this.parent.getChildAt(ic),this);
				}
			}
			
			this.drag=true;
			this.addEventListener(Event.ENTER_FRAME, fnDragCheckTouch);
			this.parent.addEventListener(MouseEvent.MOUSE_UP, fnStopDrag);

		}
		
		private function AnimEnd(){
			this.isAnimated = false;
		}
		
		public function CheckForConnect(magn:MovieClip, targMgNum:Number):Boolean{
			//trace("----------CheckThisForConnect----------");
			var ret:Boolean = false;
			var ic:Number;
			
			for(ic=0;ic<this.numChildren;ic++){
				var elem:Part = this.getChildAt(ic);
				if(magn.hitTestObject(elem["mg"+targMgNum])){
					ret = true;
				}
			}
			
			return(ret);
		}
		
		public function TestGridPosition(part:Part):Object{
			var ret:Object = {succ:false,targ:this,targmg:-1};
			
			var ic:Number;
			
			for(ic=0;ic<this.numChildren;ic++){
				var elem:Part = this.getChildAt(ic);
				
				if(elem.wPos==part.wPos&&elem.hPos==part.hPos+1){
					ret = {succ:true,targ:this, targmg:0 ,partForCon:elem};
				}
				if(elem.wPos==part.wPos&&elem.hPos==part.hPos-1){
					ret = {succ:true,targ:this, targmg:2 ,partForCon:elem};
				}
				if(elem.wPos==part.wPos+1&&elem.hPos==part.hPos){
					ret = {succ:true,targ:this, targmg:3 ,partForCon:elem};
				}
				if(elem.wPos==part.wPos-1&&elem.hPos==part.hPos){
					ret = {succ:true,targ:this, targmg:1 ,partForCon:elem};
				}
			}
			
			return(ret);
		}
		
		private function FindConnection(){
			//trace("----------FindConnection----------");
			var ic:Number;
			var ic2:Number;
			
			for(ic=0;ic<this.parent.numChildren;ic++){
				
				var elem:MovieClip = this.parent.getChildAt(ic);
				
				if(elem!=this&&elem.rotation==this.rotation){
					for(ic2=0;ic2<this.numChildren;ic2++){
						
						var elemthis:Part = this.getChildAt(ic2);
						var testObj:Object = elem.TestGridPosition(elemthis);
						if(testObj.succ){
							if(testObj.targmg==0){
								if(elem.CheckForConnect(elemthis.mg2,0)){
									this.ToGroup(elem,2,testObj.partForCon,elemthis);
									return;
								}
							}
							if(testObj.targmg==1){
								if(elem.CheckForConnect(elemthis.mg3,1)){
									this.ToGroup(elem,3,testObj.partForCon,elemthis);
									return;
								}
							}
							if(testObj.targmg==2){
								if(elem.CheckForConnect(elemthis.mg0,2)){
									this.ToGroup(elem,0,testObj.partForCon,elemthis);
									return;
								}
							}
							if(testObj.targmg==3){
								if(elem.CheckForConnect(elemthis.mg1,3)){
									this.ToGroup(elem,1,testObj.partForCon,elemthis);
									return;
								}
							}
						}
					}
				}
			}
		}
		
		private function ToGroup(targ:Group,curLocMg:Number,targPart:MovieClip,locPart:MovieClip){
			trace("----------ToGroup----------");
			
			var ic:Number;
			var tar:Array = new Array();
			var num:Number = this.numChildren
			for(ic=0;ic<num;ic++){
				trace(ic);
				var chld:MovieClip = this.getChildAt(this.numChildren-1);
				tar.push(chld);
				targ.addChild(chld);
			}
			
			var targMg:MovieClip;
			var locMg:MovieClip;
			switch(curLocMg){
				case 0:
					targMg = targPart["mg"+2];
					locMg = locPart["mg"+0];
					break;
				case 1:
					targMg = targPart["mg"+3];
					locMg = locPart["mg"+1];
					break;
				case 2:
					targMg = targPart["mg"+0];
					locMg = locPart["mg"+2];
					break;
				case 3:
					targMg = targPart["mg"+1];
					locMg = locPart["mg"+3];
					break;
			}
			
			
			var pTargMgL:Point = new Point(targMg.x,targMg.y);
			var pLocMgL:Point = new Point(locMg.x,locMg.y);
			
			var pTargMgG:Point = targPart.localToGlobal(pTargMgL);
			var pLocMgG:Point = locPart.localToGlobal(pLocMgL);
			
			pTargMgL = targ.globalToLocal(pTargMgG);
			pLocMgL = targ.globalToLocal(pLocMgG);
			var dx:Number = pTargMgL.x - pLocMgL.x;
			var dy:Number = pTargMgL.y - pLocMgL.y;
			
			for(ic=0;ic<tar.length;ic++){
				tar[ic].x += dx;
				tar[ic].y += dy;
				//trace("tar: "+ic+" x: "+tar[ic].x+" y: "+tar[ic].y);
			}
			targ.SetNewPosition();
			this.parent.removeChild(this);
		}
		
		public function SetNewPosition(){
			var ic:Number;
			var minPosX:Number = 100;
			var minPosY:Number = 100;
			var maxPosX:Number = -1;
			var maxPosY:Number = -1;
			var elem:Part;
			for(ic=0;ic<this.numChildren;ic++){
				elem = this.getChildAt(ic);
				if(elem.x<minPosX)minPosX=elem.x;
				if(elem.x>maxPosX)maxPosX=elem.x;
				if(elem.y<minPosY)minPosY=elem.y;
				if(elem.y>maxPosY)maxPosY=elem.y;
			}
			
			trace("maxPosX: "+maxPosX+" minPosX: "+minPosX+" maxPosY: "+maxPosY+" minPosY: "+minPosY);
			var dW = ((maxPosX+minPosX)/2);
			var dH = ((maxPosY+minPosY)/2);
			trace("dW: "+dW+" dH: "+dH);
			var rad:Number = this.rotation*Math.PI/180;
			trace("this.rotation: "+this.rotation+" sin: "+Math.sin(rad)+" cos: "+Math.cos(rad));
			this.x += dW*Math.cos(rad)-dH*Math.sin(rad);
			this.y += dH*Math.cos(rad)+dW*Math.sin(rad);
			
			for(ic=0;ic<this.numChildren;ic++){
				elem = this.getChildAt(ic);
				elem.x -= dW;
				elem.y -= dH;
			}
		}
		
		
		public function fnStopDrag(e:Event=null){
			this.parent.removeEventListener(MouseEvent.MOUSE_UP, fnStopDrag);
			var ic:Number;
			var ic2:Number;
			var ic3:Number;
			var tempArr:Array = new Array();
			
			var dx:Number;
			var dy:Number;
			var ddx:Number;
			var ddy:Number;
			
			var dMoved:Boolean = false;
			
			this.drag=false;
			if(this.hasEventListener(Event.ENTER_FRAME)){
				this.removeEventListener(Event.ENTER_FRAME, fnDragCheckTouch)
			}

			if(this.preX == this.x&&this.preY == this.y){
				if(!this.isAnimated){
					var targrot:Number = this.rotation + 90;
					this.isAnimated = true;
					Tweener.addTween(this,{rotation:targrot, time:0.3,  transition:"easeOutQuad",onComplete:AnimEnd});
				}
			}else{
				this.FindConnection();
			}
		}
		
		function fnDragCheckTouch(e:Event){
			if(this.drag){
				this.x = this.parent.mouseX - dragPointX;
				this.y = this.parent.mouseY - dragPointY;
			}
		}
		//----------------------------------------------------------------------------------------

	}
	
}
